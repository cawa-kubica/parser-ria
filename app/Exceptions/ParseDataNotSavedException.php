<?php

namespace App\Exceptions;

/**
 * Class ParseDataNotSavedException
 *
 * @package App\Exceptions
 */
class ParseDataNotSavedException extends BaseException
{
    /**
     * Default exception message
     */
    protected $message = 'Ошибка сохранения данных';

    /**
     * Default exception code
     */
    protected $code = 500;
}