<?php

namespace App\Exceptions;

/**
 * Class FileNotCreatedException
 *
 * @package App\Exceptions
 */
class FileNotCreatedException extends BaseException
{
    /**
     * Default exception message
     */
    protected $message = 'Ошибка сохранения файла';

    /**
     * Default exception code
     */
    protected $code = 500;
}