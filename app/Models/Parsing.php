<?php

namespace App\Models;

use App\Models\User\UserProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Parsing
 * @package App\Models
 */
class Parsing extends Model
{
    protected $table = 'parsing';
}
