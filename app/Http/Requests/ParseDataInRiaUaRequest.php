<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ParseDataInRiaUaRequest
 *
 * @package App\Http\Requests
 */
class ParseDataInRiaUaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:1|max:200|string|unique:parsing,name',
            'link'      => 'required|string|min:10',
        ];
    }
}
