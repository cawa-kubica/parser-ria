<?php

namespace App\Http\Controllers;

use App\Exceptions\FileNotCreatedException;
use App\Exceptions\ParseDataNotSavedException;
use App\Http\Requests\ParseDataInRiaUaRequest;
use App\Models\Parsing;
use Goutte\Client;
use Illuminate\Support\Facades\Storage;

/**
 * Class ParsingController
 *
 * @package App\Http\Controllers
 */
class ParsingController extends Controller
{
    private $adCount = 0;

    public function index()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $parsedData = Parsing::get()->take(5)->sortBy('created_at');

        return view('parse.index', [
            'parsedData' => $parsedData,
        ]);
    }

    public function parseAutoRia(ParseDataInRiaUaRequest $request)
    {
        try {
            $client = new Client();
            $data = $client->request('GET', $request->input('link'));

            $dataUrls = $data->filter('.ticket-title')->each(function ($node) {
                /** @noinspection PhpUndefinedMethodInspection */
                return $node->html();
            });

            $linksData = [];
            foreach (array_filter($dataUrls) as $item) {
                preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $item, $itemLink);

                if (!empty($itemLink)) {
                    $linksData[] .= $itemLink['href'][0];

                    $this->adCount++;
                }
            }

            $phonesData = [];
            foreach ($linksData as $link) {
                $data = $client->request('GET', $link);

                if($dataUrls = $data->filter('.conversion_phone_used > span')->getNode(0)) {
                    $phoneBlock = explode(' ', $dataUrls->textContent);
                    $phone = $phoneBlock[4] . $phoneBlock[5] . $phoneBlock[6];
                    $phonesData[] .= str_replace(['(', ')', '-'], '', $phone);
                }
            }

            $phonesData = array_unique(array_filter($phonesData));

            $filename = basename($request->input('name') . '-' . date('D-m-y') . '.txt');

            /** @noinspection PhpUndefinedMethodInspection */
            if (!Storage::disk('local')->put('public/parse-data/' . $filename, implode(PHP_EOL, $phonesData))) {
                throw new FileNotCreatedException();
            }

            $parse = new Parsing();
            $parse->name = $request->input('name');
            $parse->file = $filename;
            $parse->count = count($phonesData);

            if(!$parse->save()) {
                throw new ParseDataNotSavedException();
            }

            return response()->json(['success' => 'Данные добавлены'], 200);

        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
        }
    }

}