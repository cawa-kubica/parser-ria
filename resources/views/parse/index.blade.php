<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ТЗ Парсинг с auto.ria</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>


<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-5">
            <h2>Последние парсинги</h2>
            <table class="table last-parsed">
                <tbody>
                @if($parsedData->isNotEmpty())
                    @foreach($parsedData as $data)
                        <tr>
                            <td>{{ $data['name'] }}</td>
                            <td><a href="{{ url('/') }}/storage/parse-data/{{ $data['file'] }}">{{ $data['file'] }}</a> ({{ $data['count'] }})</td>
                            <td>{{ !empty($data['created_at']) ? $data['created_at']->format('d-m-y H:i') : '' }}</td>
                        </tr>
                    @endforeach
                @else
                    <p>Ранние данные отсутствуют</p>
                @endif
                </tbody>
            </table>
        </div>
        <div class="col-md-7">
            <h2>Начать новый парсинг</h2>
            <div class="parse-form">
                <form>
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Название сессии</label>
                        <input type="text" class="form-control" id="name" placeholder="Название сессии">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Ссылка (с auto.ria)</label>
                        <input type="text" class="form-control" id="link" placeholder="https://auto.ria.com/">
                    </div>
                    <button type="button" id="start-parse-ria" class="btn btn-lg btn-success">Запустить</button>
                </form>
            </div>
        </div>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/js/parse.js"></script>
</body>
</html>
