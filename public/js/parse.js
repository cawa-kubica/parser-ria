function parseInRia() {
    let link = $('#link').val();
    let name = $('#name').val();

    if(link.length < 1 || name.length < 10 || name.length > 255) {
        alert('Необходимо заполнить поля ссылка и название');
    }
    console.log('send data: ', link, name);

    $.ajax({
        url: "/parse/ria",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "link": link,
            "name": name,
        },
        beforeSend: function () {
            $("#start-parse-ria").prop('value', 'Загрузка данных');
            $("#start-parse-ria").prop('disabled', true);
            $('.parse-form').append('<img src="https://loading.io/spinners/blocks/lg.rotating-squares-preloader-gif.gif">');
        },
        success: function (response) {
            if (response.success) {
                location.reload();
            }
            else {
                toastr.error(response.errors[0]);
                return;
            }
        }
    });
}

$(document).ready(function () {
    $('#start-parse-ria').on('click', function () {
        parseInRia();
    });
});